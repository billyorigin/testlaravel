@extends('layouts.app')

@section('content')
    <link href="{{ url('css') }}/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('css') }}/font-awesome.min.css" rel="stylesheet" type="text/css" />



    <div class="row">
        @if($user->is_admin)
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div id="info"></div>
                        <button data-toggle="modal" data-target="#myModal" type="button" class="btn btn-success">Add Angsuran Rumah</button>
                        <br>
                            <table id="angsuran" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Harga Rumah</th>
                                    <th>Maksimal Tahun Angsuran</th>
                                    <th>Created At</th>
                                    <th>Fungsi</th>
                                </tr>
                                </thead>
                                <tbody>


                                </tbody>
                            </table>

                            <div id="myModal_edit" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" id="angsuran_title">Edit Data Angsuran</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form method="post" action="">
                                            <div class="form-group">
                                            <label>Harga Rumah :</label>
                                            <input type="number" required="required" id="harga_rumah_edit" class="form-control" name="harga_rumah">
                                            </div>

                                            <div class="form-group">
                                                <label>Max Tahun Angsuran :</label>
                                                <input type="number" required="required" id="max_tahun_edit" class="form-control" name="max_tahun">
                                            </div>
                                            <button type="button" onclick="angsuran_update(this.value)" id="angsuran_button_edit" class="btn btn-primary btn-block">Update</button>
                                        </form>


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="submit_angsuran" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" id="angsuran_title">Tambah Data Angsuran</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form method="post" action="">
                                                <div class="form-group">
                                                    <label>Harga Rumah :</label>
                                                    <input type="number" required="required" id="harga_rumah" class="form-control" name="harga_rumah">
                                                </div>

                                                <div class="form-group">
                                                    <label>Max Tahun Angsuran :</label>
                                                    <input type="number" required="required" id="max_tahun" class="form-control" name="max_tahun">
                                                </div>
                                                <button type="button" onclick="add_angsuran()" class="btn btn-primary btn-block">Submit</button>
                                            </form>


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="submit_angsuran" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        <div id="user_edit_modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="angsuran_title">Edit User</h4>
                                    </div>

                                    <div class="modal-body">
                                        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label for="name" class="col-md-4 control-label">Name</label>

                                                <div class="col-md-6">
                                                    <input id="name" type="text" class="form-control" name="name" required autofocus>

                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" readonly="readonly" class="form-control" name="email" required>

                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">Address</label>

                                                <div class="col-md-6">
                                                    <input id="address" type="text" class="form-control" name="address"  required>


                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">Job</label>

                                                <div class="col-md-6">
                                                    <input id="job" type="text" class="form-control" name="job"  required>


                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">Salary</label>

                                                <div class="col-md-6">
                                                    <input id="salary" type="number" class="form-control" name="salary"  required>


                                                </div>
                                            </div>


                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="col-md-4 control-label">Password</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control" name="password" required>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="button" onclick="update_user(this.value)" id="update_user_button" class="btn btn-primary">
                                                        Update User
                                                    </button>
                                                </div>
                                            </div>
                                        </form>



                                    </div>
                                    <div class="modal-footer">
                                        <div id="error_user"></div>
                                        <button type="button" id="submit_angsuran" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>





                    {{-- url('images', $user->npwp) --}}

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">User Management</div>

                <table id="user" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Penghasilan</th>
                        <th>Alamat</th>
                        <th>Detail</th>
                    </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>

                <div class="panel-body">
                </div>
            </div>
        </div>


        </div>

    @else

            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form method="post">
                            <div class="form-group">
                                <label>Gaji Bersih Anda</label>
                        <input class="form-control" id="gaji_anda" required="required" value="{{$user->penghasilan}}">
                            </div>

                            <div class="form-group">
                                <label>Pilih Harga Rumah</label>
                                <select required="required" id="harga_rumah_select" class="form-control">
                                    @foreach($angsuran as $row)
                                        <option value="{{$row->id}}">{{number_format($row->harga_rumah)}} - Hingga {{$row->max_tahun}} Tahun</option>
                                    @endforeach
                                </select>
                            </div>
                        <button type="button" id="button_angsur" onclick="hitung_angsuran()" class="btn btn-primary">Hitung Angsuran</button>
                        </form>
                        <div id="pesan" ></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Lama Tahun</th>
                                <th>Cicilan Perbulan</th>
                            </tr>
                            </thead>
                            <tbody id="hasil">

                            </tbody>
                        </table>

                    </div>

                </div>

            </div>

    @endif
    </div>

@push('scripts')
<script src="{{url('js')}}/jquery.dataTables.min.js" charset="utf-8"></script>
<script src="{{url('js')}}/sweetalert2.all.js" charset="utf-8"></script>

@if($user->is_admin)
<script type="text/javascript">
    $(function(){
        $('#angsuran').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                method : 'GET',
                url : '{{route('angsuran.index')}}'
            },
            columns: [
                { data: 'harga_rumah', name: 'harga_rumah' },
                { data: 'max_tahun', name: 'max_tahun' },
                { data: 'created_at', name: 'created_at' },
                { data: 'detail', name: 'detail' }
            ]
        });
    });

    $(function(){
        $('#user').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                method : 'GET',
                url : '{{route('user.index')}}'
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'penghasilan', name: 'penghasilan' },
                { data: 'alamat', name: 'alamat' },
                { data: 'detail', name: 'detail' }
            ]
        });
    });
    
    function add_angsuran() {
        var formdata = {
            'harga_rumah' : $('#harga_rumah').val(),
            'max_tahun' : $('#max_tahun').val(),
            '_token' : '{{csrf_token()}}'
        };
        $('#submit_angsuran').text('Loading...');
        $.ajax({
            method : 'POST',
            url : '{{route('angsuran.store')}}',
            data : formdata,
            cache : false,
            dataType : 'json',
            encode : true
        })
            .done(function (data){
                var table = $('#angsuran').DataTable();
                table.ajax.reload( null, false );
                $('#myModal').modal('toggle');
                $('#info').html(data.info);

            });
    }

    function angsuran_edit(id) {
        $.ajax({
            method : 'GET',
            url : '{{url("angsuran")}}/'+id+'/edit',
            cache : false,
            dataType : 'json',
            encode : true
        })
            .done(function (data){
                var table = $('#angsuran').DataTable();
                table.ajax.reload( null, false );
                $('#myModal_edit').modal('show');
                $('#harga_rumah_edit').val(data.harga_rumah);
                $('#max_tahun_edit').val(data.max_tahun);
                $('#angsuran_button_edit').val(data.id);

            });
    }

    function user_edit(id) {
        $.ajax({
            method : 'GET',
            url : '{{url("user")}}/'+id+'/edit',
            cache : false,
            dataType : 'json',
            encode : true
        })
            .done(function (data){
                var table = $('#user').DataTable();
                table.ajax.reload( null, false );
                $('#user_edit_modal').modal('show');
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#address').val(data.address);
                $('#salary').val(data.salary);
                $('#job').val(data.job);
                $('#update_user_button').val(data.id);

            });
    }

    function angsuran_update(id) {
        var formdata = {
            'harga_rumah' : $('#harga_rumah_edit').val(),
            'max_tahun' : $('#max_tahun_edit').val(),
            '_token' : '{{csrf_token()}}',
            'id' : id
        };
        $('#submit_angsuran').text('Loading...');
        $.ajax({
            method : 'PUT',
            url : '{{url("angsuran")}}/'+id,
            data : formdata,
            cache : false,
            dataType : 'json',
            encode : true
        })
            .done(function (data){
                var table = $('#angsuran').DataTable();
                table.ajax.reload( null, false );
                $('#myModal_edit').modal('toggle');
                $('#info').html(data.info);

            });

    }

    function update_user(id) {

        if($('#password').val() != $('#password-confirm').val())
        {

        }
        else
        {
        var formdata = {
            'name' : $('#name').val(),
            'email' : $('#email').val(),
            'salary' : $('#salary').val(),
            'job' : $('#job').val(),
            'address' : $('#address').val(),
            'password' : $('#password').val(),
            'password_confirmation' : $('#password-confirm').val(),

            '_token' : '{{csrf_token()}}',
            'id' : id
        };
        $('#submit_angsuran').text('Loading...');
        $.ajax({
            method : 'PUT',
            url : '{{url("user")}}/'+id,
            data : formdata,
            dataType : 'json',
            encode : true
        })
            .done(function (data){
                var table = $('#angsuran').DataTable();
                table.ajax.reload( null, false );
                $('#user_edit_modal').modal('toggle');
                $('#info').html(data.info);

            }).fail(function (data) {
                $('#error_user').text(JSON.stringify(data.responseJSON.errors));
            console.log(data.responseJSON);
        });
    }}

    function delete_angsuran(id) {
        swal({
            title: 'Yakin Hapus?',
            text: "Data Akan Hilang Jika Dihapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!'
        }).then((result) => {
            if (result.value) {

            $.ajax({
                method : 'DELETE',
                url : '{{url("angsuran")}}/'+id,
                data : {'_token' : '{{csrf_token()}}'},
                cache : false,
                dataType : 'json',
                encode : true
            })
                .done(function (data){
                    var table = $('#angsuran').DataTable();
                    table.ajax.reload( null, false );
                    swal(
                        'Terhapus!',
                        'Data Telah Terhapus.',
                        'success'
                    )

                });



        }
    })
    }

    function user_delete(id) {
        swal({
            title: 'Yakin Hapus?',
            text: "Data Akan Hilang Jika Dihapus!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!'
        }).then((result) => {
            if (result.value) {

            $.ajax({
                method : 'DELETE',
                url : '{{url("user")}}/'+id,
                data : {'_token' : '{{csrf_token()}}'},
                cache : false,
                dataType : 'json',
                encode : true
            })
                .done(function (data){
                    var table = $('#user').DataTable();
                    table.ajax.reload( null, false );
                    swal(
                        'Terhapus!',
                        'Data Telah Terhapus.',
                        'success'
                    )

                });



        }
    })
    }
    
</script>
    @else

    <script type="text/javascript">

        function hitung_angsuran() {
            $('#pesan').html('');
            $('#hasil').html('');
            var formdata = {
                'rumah' : $('#harga_rumah_select').val(),
                'gaji' : $('#gaji_anda').val(),
                '_token' : '{{csrf_token()}}'
            };
            $('#button_angsur').text('Loading...');
            $.ajax({
                method : 'POST',
                url : '{{url('hitung_angsuran')}}',
                data : formdata,
                cache : false,
                dataType : 'json',
                encode : true
            })
                .done(function (data){

                    $('#button_angsur').text('Hitung Angsuran');
                    $('#hasil').html(data.angsuran);
                    $('#pesan').html(data.pesan);

                });
        }

    </script>

@endif



@endpush
@endsection
