<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //sdasd
        User::create([
            'name' => 'admin',
            'email' => 'admin@email.com',
            'password' => bcrypt('secret'),
            'is_admin' => 1
        ]);
    }
}
