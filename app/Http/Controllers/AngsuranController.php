<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Angsuran;

class AngsuranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        //
        $user = \Auth::user();
        if(!$user->is_admin)
            abort(404);
        if($request->ajax())
        {
            $angsuran = Angsuran::all();

            return \DataTables::of($angsuran)
                ->addColumn('detail', function ($item){
                    return "
<button onclick='angsuran_edit(this.value)' value='$item->id' href='' class='btn btn-sm btn-info'>Edit</button>
<button onclick='delete_angsuran(this.value)' value='$item->id' href='' class='btn btn-sm btn-danger'>Hapus</button>";
                    
                })
                ->escapeColumns([])
                ->make();

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->ajax())
        {
            $angsuran = new Angsuran();
            $angsuran->harga_rumah = $request->harga_rumah;
            $angsuran->max_tahun = $request->max_tahun;
            $angsuran->save();

            $data['info'] = "<div class='alert alert-success'>Data Angsuran Rumah Telah Ditambahkan </div>";
            return response()->json($data);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //
        if($request->ajax()) {
            $angsuran = Angsuran::findOrFail($id);

            $data['harga_rumah'] = $angsuran->harga_rumah;
            $data['max_tahun'] = $angsuran->max_tahun;
            $data['id'] = $angsuran->id;

            return response()->json($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($request->ajax())
        {
            $angsuran = Angsuran::findOrFail($id);
            $angsuran->harga_rumah = $request->harga_rumah;
            $angsuran->max_tahun = $request->max_tahun;
            $angsuran->save();

            $data['info'] = "<div class='alert alert-success'>Data Angsuran Rumah Telah Diperbaharui </div>";
            return response()->json($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $angsuran = Angsuran::findOrFail($id);
        $angsuran->delete();

        $data['info'] = "<div class='alert alert-success'>Data Angsuran Rumah Telah Dihapus </div>";
        return response()->json($data);
    }
}
