<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Angsuran;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['user'] = \Auth::user();
        $data['angsuran'] = Angsuran::all();
        return view('home', $data);
    }

    public function hitung_angsuran(Request $request)
    {
        $angsuran = Angsuran::findOrFail($request->rumah);

        //$bulan = $angsuran->max_tahun * 12;

        $hasil = [];
        for($x = 1; $x <= $angsuran->max_tahun+1; $x++)
        {
            $aaa = $angsuran->harga_rumah / ($x * 12);
            $bbb = number_format($aaa);
            if(floatval($aaa) >= floatval($request->gaji) ) {

                $hasil[] = "<tr>
                        <td>$x</td>
                        <td>Rp.$bbb</td>
                        </tr>";
            }
            else {
                $hasil[] = "<tr class='alert-success'>
                        <td>$x</td>
                        <td>Rp.$bbb</td>
                        </tr>";
                if($x == $angsuran->max_tahun)
                    break;
            }
            if($x > $angsuran->max_tahun)
            {
                $data['pesan'] = "<div class='alert alert-danger'>Gaji Anda Masih Kurang Untuk Mencicil Rumah Ini </div>";
                break;
            }

        }


        $data['angsuran'] = $hasil;
        return response()->json($data);

    }
}
