<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $user = \Auth::user();
        if(!$user->is_admin)
            abort(404);
        if($request->ajax())
        {
            $users = User::all();

            return \DataTables::of($users)
                ->addColumn('detail', function ($item){
                    $url = url('images',$item->npwp);
                    if($item->is_admin == false) {
                        return "
<a href='$url' target='_blank' class='btn btn-sm btn-info'>NPWP</a>
<button onclick='user_edit(this.value)' value='$item->id' href='' class='btn btn-sm btn-info'>Edit</button>
<button onclick='user_delete(this.value)' value='$item->id' href='' class='btn btn-sm btn-danger'>Hapus</button>";
                    }
                    else
                        return '';
                })
                ->escapeColumns([])
                ->make();

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);

        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['address'] = $user->alamat;
        $data['salary'] = $user->penghasilan;
        $data['job'] = $user->pekerjaan;
        $data['id'] = $user->id;

        return response()->json($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'required',
            'salary' => 'required|numeric|min:7000000',
            'job' => 'required'
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->password = $request->password;
        $user->alamat = $request->address;
        $user->penghasilan = $request->salary;
        $user->pekerjaan = $request->job;
        $user->save();

        $data['info'] = "<div class='alert alert-success'>Data User Telah Diubah </div>";
        return response()->json($data);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $angsuran = User::findOrFail($id);
        $angsuran->delete();

        $data['info'] = "<div class='alert alert-success'>Data User Telah Dihapus </div>";
        return response()->json($data);
    }
}
